from unittest import TestCase
from Calculadora import Calculadora

class CalculadoraTest(TestCase):
    def test_sumar(self):
        self.assertEqual(Calculadora().sumar(""),0,"Cadena Vacia")

    def test_sumar_unaCadena(self):
        self.assertEqual(Calculadora().sumar("1"),1,"Un Numero")
    def test_sumar_cadenaConUnNumero(self):
        self.assertEqual(Calculadora().sumar("1"),1,"Un Numero")
        self.assertEqual(Calculadora().sumar("2"),2,"Un Numero")

    def test_sumar_cadenaConDosNumeros(self):
        self.assertEqual(Calculadora().sumar("1,3"),4,"Dos Numeros")

    def test_sumar_cadenaConVariosNumeros(self):
        self.assertEqual(Calculadora().sumar("1,2,3,4,5"),15,"Varios Numeros")

    def test_sumar_cadenaConVariosNumerosVariosSeparadores(self):
        self.assertEqual(Calculadora().sumar("1,2&3:4:5"),15,"Varios Separadores")